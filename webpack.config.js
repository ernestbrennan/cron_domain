var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .addEntry('js/script', './assets/js/script.js')
    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/uikit', './assets/js/uikit.min.js')

    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())
    .enableReactPreset()
;

module.exports = Encore.getWebpackConfig();
