<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 27.02.2018
 * Time: 11:56
 */

namespace App\Controller;


use App\Entity\Category;
use App\Entity\Domains;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends Controller{

	/**
	 * @Route("/add-category", methods={"POST"})
	 */
	public function addCategory( Request $request ) {
		$categoryName = $request->get( 'category_name' );

		$em     = $this->getDoctrine()->getManager();
		$category = new Category();
		$category->setName( $categoryName );
		$em->persist($category);

		$em->flush();

		return $this->json( $category->getId() );
	}

	/**
	 * @Route("/delete-category", methods={"POST"})
	 */
	public function deleteCategory( Request $request ) {
		$categoryId = $request->get( 'category_id' );
		$em = $this->getDoctrine()->getManager();
		$categoryRep = $this->getDoctrine()->getRepository(Category::class);
		$domainsRep = $this->getDoctrine()->getRepository( Domains::class );

		$category = $categoryRep->find($categoryId);
		$domains = $domainsRep->findBy(['category' => $categoryId]);
		foreach ($domains as $item){
			$item->setCategory( null );
		}
		$em->remove($category);

		$em->flush();

		return $this->json('done'  );
	}

	/**
	 * @Route("/add-category-to-domain", methods={"POST"})
	 */
	public function addCategoryToDomain( Request $request ) {
		$categoryId = $request->get( 'category_id' );
		$domainId = $request->get( 'domain_id' );

		$domainsRep = $this->getDoctrine()->getRepository( Domains::class );
		$domain = $domainsRep->find($domainId);
		$domain->setCategory( $categoryId );

		$em = $this->getDoctrine()->getManager();

		$em->persist( $domain );
		$em->flush();

		return $this->json( $domain->getCategory());
	}

}