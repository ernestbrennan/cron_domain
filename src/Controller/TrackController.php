<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 27.02.2018
 * Time: 11:59
 */

namespace App\Controller;


use App\Entity\Category;
use App\Entity\Domains;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TrackController extends Controller {

	/**
	 * @Route("/track", methods={"POST"})
	 */
	public function changeTrack( Request $request ) {
		$domainId = $request->get( 'track' );
		$domainsRep = $this->getDoctrine()->getRepository( Domains::class );
		$domain = $domainsRep->find($domainId);
		$em = $this->getDoctrine()->getManager();
		$trackStatus = $domain->getTrack();

		$domain->setTrack(!$trackStatus);
		$em->persist( $domain );
		$em->flush();

		return $this->json($trackStatus);
	}
	/**
	 * @Route("/track-domain", methods={"POST"})
	 */
	public function changeTrackDomain( Request $request ) {
		$domainId = $request->get( 'track_domain' );
		$domainsRep = $this->getDoctrine()->getRepository( Domains::class );
		$domain = $domainsRep->find($domainId);
		$trackStatus = $domain->getTrackDomain();
		$em = $this->getDoctrine()->getManager();

		$domain->setTrackDomain(!$trackStatus);
		$em->persist( $domain );
		$em->flush();

		return $this->json($trackStatus);
	}
	/**
	 * @Route("/track-ip", methods={"POST"})
	 */
	public function changeTrackIp( Request $request ) {
		$domainId = $request->get( 'track_ip' );
		$em = $this->getDoctrine()->getManager();
		$domainsRep = $this->getDoctrine()->getRepository( Domains::class );
		$domain = $domainsRep->find($domainId);
		$trackStatus = $domain->getTrackIp();

		$domain->setTrackIp(!$trackStatus);
		$em->persist( $domain );
		$em->flush();

		return $this->json($trackStatus);
	}
}