<?php

namespace App\Controller;

use App\Entity\Phone;
use App\Entity\Category;
use App\Entity\Domains;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DomainController extends Controller {

	/**
	 * @Route("/add-domain", methods={"POST"})
	 */
	public function addDomain( Request $request, LoggerInterface $logger ) {

		$checkDC = new CheckDomainsController();
		$domain  = $request->get( 'domain_url' );
		$category_id  = $request->get( 'category_id' );
		$phone_id  = $request->get( 'phone_id' );

		$new_domainDB = array(
			'url'           => $domain,
			'is_ip_ban'     => false,
			'is_domain_ban' => false,
			'category_id'   => $category_id,
			'phone_id'      => $phone_id,
			'track'         => false,
			'track_domain'  => false,
			'track_ip'      => false,
		);
		if ( filter_var( $domain, FILTER_VALIDATE_IP ) ) {
			$respIP = $checkDC->checkAntizapretSite( $domain );

			if ( isset( $respIP->register ) ) {
				$new_domainDB['is_ip_ban'] = true;
			}
			$new_domainDB['ip'] = $domain;
		} else {
			$ip = $this->checkIpByDomain($domain);
			$respIP = $checkDC->checkAntizapretSite( $ip );
			$respDomain = $checkDC->checkAntizapretSite( $domain );

			if ( isset( $respIP->register ) ) {
				$new_domainDB['is_ip_ban'] = true;
			}
			if ( isset( $respDomain->register ) ) {
				$new_domainDB['is_domain_ban'] = true;
			}

			$new_domainDB['ip'] = $ip;
		}

		$id = $this->saveToDb( $new_domainDB );
		$new_domainDB['id'] = $id;
		return $this->json( $new_domainDB );
	}

	/**
	 * @Route("/delete-domain", methods={"POST"})
	 */
	public function deleteDomain( Request $request ) {
		$id = $request->get( 'domain_id' );
		$em     = $this->getDoctrine()->getManager();
		$domain = $em->getRepository( 'App:Domains' )->find( $id );

		$em->remove($domain);
		$em->flush();
		return $this->json($domain);
	}

	/**
	 * @Route("/get-by-category", methods={"POST"})
	 */
	public function getByCategory( Request $request ) {
		$categoryId     = $request->get( 'category_id' );
		$domainsRep     = $this->getDoctrine()->getRepository(Domains::class );
		$categoryRep    = $this->getDoctrine()->getRepository(Category::class);
		$phoneRep       = $this->getDoctrine()->getRepository(Phone::class);
		
		$categories     = $categoryRep->find($categoryId);
		$domains        = $domainsRep->findBy(['category' => $categoryId]);
		$phones         = $phoneRep->findAll();

		$domainArr      = array();
		foreach ( $domains as $domain ) {
			$phoneId        = null;
			$phoneNumber    = null;
			foreach ($phones as $phone){
				if ( $phone->getId() === $domain->getPhone() ) {
					$phoneNumber    = $phone->getPhoneNumber();
					$phoneId        = $phone->getId();
				}
			}
			$domainArr[] = [
				'id'            =>  $domain->getId(),
				'url'           =>  $domain->getUrl(),
				'ip'            =>  $domain->getIp(),
				'ban_domain'    =>  $domain->getisDomainBan(),
				'ban_ip'        =>  $domain->getisIpBan(),
				'category_name' =>  $categories->getName(),
				'category_id'   =>  $categoryId,
				'phone_id'      =>  $phoneId,
				'phone_number'  =>  $phoneNumber,
				'track'         =>  $domain->getTrack(),
				'track_ip'      =>  $domain->getTrackIp(),
				'track_domain'  =>  $domain->getTrackDomain(),
			];
		}
		return $this->json($domainArr );
	}

	/**
	 * @Route("/get-by-phone", methods={"POST"})
	 */
	public function getByPhone( Request $request ) {
		$phoneId        = $request->get( 'phone_id' );

		$domainsRep     = $this->getDoctrine()->getRepository(Domains::class );
		$categoryRep    = $this->getDoctrine()->getRepository(Category::class);
		$phoneRep       = $this->getDoctrine()->getRepository(Phone::class);

		$phones         = $phoneRep->find($phoneId);
		$domains        = $domainsRep->findBy(['phone' => $phoneId]);
		$categories     = $categoryRep->findAll();

		$domainArr      = array();
		foreach ( $domains as $domain ) {
			$categoryId        = null;
			$categoriesName    = null;
			foreach ($categories as $category){
				if ( $category->getId() === $domain->getCategory() ) {
					$categoriesName = $category->getName();
					$phoneId        = $category->getId();
				}
			}
			$domainArr[] = [
				'id'            =>  $domain->getId(),
				'url'           =>  $domain->getUrl(),
				'ip'            =>  $domain->getIp(),
				'ban_domain'    =>  $domain->getisDomainBan(),
				'ban_ip'        =>  $domain->getisIpBan(),
				'category_name' =>  $categoriesName,
				'category_id'   =>  $categoryId,
				'phone_id'      =>  $phoneId,
				'phone_number'  =>  $phones->getPhoneNumber(),
				'track'         =>  $domain->getTrack(),
				'track_ip'      =>  $domain->getTrackIp(),
				'track_domain'  =>  $domain->getTrackDomain(),
			];
		}
		return $this->json($domainArr );
	}

	/**
	 * @Route("/get-all-domain", methods={"POST"})
	 */
	public function getAllDomains( Request $request ) {
		$domainsRep     = $this->getDoctrine()->getRepository( Domains::class );
		$categoryRep    = $this->getDoctrine()->getRepository(Category::class);
		$phoneRep       = $this->getDoctrine()->getRepository(Phone::class);

		$category       = $categoryRep->findAll();
		$domains        = $domainsRep->findAll();
		$phones         = $phoneRep->findAll();

		$domainArr = array();
		foreach ( $domains as $domain ) {
			$category_name  = null;
			$category_id    = null;
			$phoneId        = null;
			$phoneNumber    = null;
			foreach ($phones as $phone){
				if ( $phone->getId() === $domain->getPhone() ) {
					$phoneNumber    = $phone->getPhoneNumber();
					$phoneId        = $phone->getId();
				}
			}
			foreach ($category as $cat){
				if ( $cat->getId() === $domain->getCategory() ) {
					$category_name  = $cat->getName();
					$category_id    = $cat->getId();
				}
			}
			$domainArr[] = [
				'id'            =>  $domain->getId(),
				'url'           =>  $domain->getUrl(),
				'ip'            =>  $domain->getIp(),
				'ban_domain'    =>  $domain->getisDomainBan(),
				'ban_ip'        =>  $domain->getisIpBan(),
				'category_name' =>  $category_name,
				'category_id'   =>  $category_id,
				'phone_id'      =>  $phoneId,
				'phone_number'  =>  $phoneNumber,
				'track'         =>  $domain->getTrack(),
				'track_ip'      =>  $domain->getTrackIp(),
				'track_domain'  =>  $domain->getTrackDomain(),

			];
		}
		return $this->json($domainArr );
	}

	public function checkIpByDomain( $domain ) {
		return gethostbyname( $domain );
	}

	public function saveToDb( $arr ) {
		$em     = $this->getDoctrine()->getManager();
		$domain = new Domains();
		$domain
			->setUrl( $arr['url'] )
			->setIp( $arr['ip'] )
			->setIsIpBan( $arr['is_ip_ban'] )
			->setIsDomainBan( $arr['is_domain_ban'] );

		$domain->setTrack($arr['track']);
		$domain->setTrackDomain($arr['track_domain']);
		$domain->setTrackIp( $arr['track_ip'] );

		if ( $arr['category_id'] != 'null' ) {
			$domain->setCategory( $arr['category_id'] );
		}
		if ( $arr['phone_id'] != 'null' ) {
			$domain->setPhone( $arr['phone_id'] );
		}
		$em->persist( $domain );

		$em->flush();

		return $domain->getId();
	}
}