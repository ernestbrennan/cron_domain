<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 14.02.2018
 * Time: 11:17
 */

namespace App\Controller;

use App\Entity\Domains;
use SoapClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GuzzleHttp\Client;

class CheckDomainsController extends Controller{

	public function checkAntizapretSite($query){
		$client = new Client( [ 'base_uri' => 'http://api.antizapret.info' ] );
		$resp   = $client->request( 'GET', 'get.php', [
			'query' => [
				'item' => $query,
				'type' => 'json'
			]
		] );

		return json_decode($resp->getBody()->getContents());
	}

	public function checkRuBlacklistSite(){
		$client = new Client( [ 'base_uri' => 'https://reestr.rublacklist.net' ] );
		$resp   = $client->request( 'GET', 'api/v2/current/csv' );
		return json_decode($resp->getBody()->getContents());
	}

	public function sendMessageWithBan($arr){
		try {
			$client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');

			$auth = [
				'login'     => 'olegicnin20072',
				'password'  => 'oleg2810'
			];

			$result = $client->Auth($auth);

			foreach ($arr as $sms){
				if(!empty($sms['message'])){
					$message = "Новые баны, ID записей:\r\n" .implode(",", $sms['message']);

					$text = iconv('utf-8', 'utf-8',  $message);

					$sms = [
						'sender' => 'Msg',
						'destination' => $sms['phone_number'],
						'text' => $text
					];
					$result = $client->SendSMS($sms);
				}
			}
		} catch(Exception $e) {
			echo 'Ошибка: ' . $e->getMessage() . PHP_EOL;
		}
	}



}