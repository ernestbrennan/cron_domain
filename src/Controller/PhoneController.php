<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 07.03.2018
 * Time: 12:36
 */

namespace App\Controller;

use App\Entity\Phone;
use App\Entity\Domains;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PhoneController extends Controller{

	/**
	 * @Route("/add-phone", methods={"POST"})
	 */
	public function addPhone( Request $request ) {
		$phoneNumber = $request->get( 'phone_number' );
		$em         = $this->getDoctrine()->getManager();
		$phone      = new Phone();

		$phone->setPhoneNumber( $phoneNumber );
		$em->persist($phone);
		$em->flush();

		return $this->json( $phone->getId() );
	}

	/**
	 * @Route("/delete-phone", methods={"POST"})
	 */
	public function deletePhone( Request $request ) {
		$phoneId    = $request->get( 'phone_id' );

		$em         = $this->getDoctrine()->getManager();
		$phoneRep   = $this->getDoctrine()->getRepository(Phone::class);
		$domainsRep = $this->getDoctrine()->getRepository( Domains::class );

		$phone      = $phoneRep->find($phoneId);
		$domains    = $domainsRep->findBy(['phone' => $phoneId]);
		foreach ($domains as $item){
			$item->setPhone( null );
		}
		$em->remove($phone);

		$em->flush();

		return $this->json('done'  );
	}

	/**
	 * @Route("/add-phone-to-domain", methods={"POST"})
	 */
	public function addPhoneToDomain( Request $request ) {
		$phone_id       = $request->get( 'phone_id' );
		$domainId       = $request->get( 'domain_id' );

		$em             = $this->getDoctrine()->getManager();
		$domainsRep     = $this->getDoctrine()->getRepository( Domains::class );
		$domain         = $domainsRep->find($domainId);

		$domain->setPhone( $phone_id );


		$em->persist( $domain );
		$em->flush();

		return $this->json( $domain->getPhone());
	}
}