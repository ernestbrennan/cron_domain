<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180227105643 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE domains CHANGE track track TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE track_ip track_ip TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE track_domain track_domain TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE domains CHANGE track track TINYINT(1) NOT NULL, CHANGE track_ip track_ip TINYINT(1) NOT NULL, CHANGE track_domain track_domain TINYINT(1) NOT NULL');
    }
}
