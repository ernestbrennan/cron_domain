<?php

namespace App\Repository;

use App\Entity\Domains;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DomainsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Domains::class);
    }

    public function  getTrackingDomainsAndIps(){
	    return $this->findBy([
		    'track'         => 1,
		    'track_domain'  => 1,
		    'track_ip'      => 1
	    ]);
    }

    public function getTrackingDomains()
    {
        return $this->findBy([
        	'track'         => 1,
	        'track_domain'  => 1,
	        'track_ip'      => 0
        ]);

    }
	public function getTrackingIps()
	{
		return $this->findBy([
			'track'         => 1,
			'track_ip'      => 1,
			'track_domain'  => 0
		]);
	}

}
