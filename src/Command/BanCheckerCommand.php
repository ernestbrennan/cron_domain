<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 14.02.2018
 * Time: 10:13
 */

namespace App\Command;


use App\Controller\CheckDomainsController;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BanCheckerCommand extends ContainerAwareCommand {
	private $logger;

	public function __construct( LoggerInterface $logger ) {
		parent::__construct();
		$this->logger = $logger;
	}

	protected function configure() {

		$this
			->setName( 'ban:checker' )
			->setDescription( 'check blocked domains' )
			->setHelp( 'This command allows you to create a user...' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$file = file_get_contents( 'http://reestr.rublacklist.net/api/v2/current/csv' );

		$em             = $this->getContainer()->get( 'doctrine' )->getManager();
		$checkDC        = new CheckDomainsController();

		$domainsAndIps = $this->getContainer()->get( 'doctrine' )->getRepository( 'App:Domains' )->getTrackingDomainsAndIps();
		$domains       = $this->getContainer()->get( 'doctrine' )->getRepository( 'App:Domains' )->getTrackingDomains();
		$ips           = $this->getContainer()->get( 'doctrine' )->getRepository( 'App:Domains' )->getTrackingIps();
		$phones        = $this->getContainer()->get( 'doctrine' )->getRepository( 'App:Phone' )->findAll();

		$resultArr          = [];
		$needSendMessage    = false;
		foreach ($phones as $phone) {
			$resultArr[] = [
				'phone_id'      => $phone->getId(),
				'phone_number'  => $phone->getPhoneNumber(),
				'message'       => []
			];
		}
		if ( count( $domainsAndIps ) > 0 ) {
			foreach ( $domainsAndIps as $item ) {
				$url          = $item->getUrl();
				$needUpdate   = false;
				$new_domainDB = array(
					'url'           => $url,
					'is_ip_ban'     => false,
					'is_domain_ban' => false
				);
				if ( filter_var( $url, FILTER_VALIDATE_IP ) ) {
					$respIP = $checkDC->checkAntizapretSite( $url );
					if ( isset( $respIP->register ) || strpos( $file, $url ) ) {
						$new_domainDB['is_ip_ban'] = true;
					}
					$new_domainDB['ip'] = $url;
				} else {
					$ip         = gethostbyname( $url );
					$respIP     = $checkDC->checkAntizapretSite( $ip );
					$respDomain = $checkDC->checkAntizapretSite( $url );
					if ( isset( $respIP->register ) || strpos( $file, $ip ) ) {
						$new_domainDB['is_ip_ban'] = true;
					}
					if ( isset( $respDomain->register ) || strpos( $file, $url ) ) {
						$new_domainDB['is_domain_ban'] = true;
					}
					$new_domainDB['ip'] = $ip;
				}

				if ( $item->getIp() != $new_domainDB['ip'] ) {
					$item->setIp( $new_domainDB['ip'] );
					$needUpdate = true;

				}
				if ( $item->getIsDomainBan() != $new_domainDB['is_domain_ban'] ) {
					$item->setIsDomainBan( $new_domainDB['is_domain_ban'] );
					$resultArr[array_search($item->getPhone(), array_column($resultArr, 'phone_id'))]['message'][] = $item->getId();

					$needSendMessage    = true;
					$needUpdate         = true;

				}
				if ( $item->getisIpBan() != $new_domainDB['is_ip_ban'] ) {
					$item->setIsIpBan( $new_domainDB['is_ip_ban'] );
					$resultArr[array_search($item->getPhone(), array_column($resultArr, 'phone_id'))]['message'][] = $item->getId();
					$needSendMessage    = true;
					$needUpdate         = true;
				}
				if ( $needUpdate ) {
					$em->persist( $item );
					$em->flush();
				}
			}
		}
		if ( count( $domains ) > 0 ) {
			foreach ( $domains as $item ) {
				$url          = $item->getUrl();
				$new_domainDB = array(
					'url'           => $url,
					'is_domain_ban' => false
				);
				$respDomain = $checkDC->checkAntizapretSite( $url );

				if ( isset( $respDomain->register ) || strpos( $file, $url ) ) {
					$new_domainDB['is_domain_ban'] = true;
				}

				if ( $item->getIsDomainBan() != $new_domainDB['is_domain_ban'] ) {
					$item->setIsDomainBan( $new_domainDB['is_domain_ban'] );
					$resultArr[array_search($item->getPhone(), array_column($resultArr, 'phone_id'))]['message'][] = $item->getId();
					$needSendMessage    = true;

					$em->persist( $item );
					$em->flush();
				}
			}
		}
		if ( count( $ips ) > 0 ) {
			foreach ( $ips as $item ) {
				$ip           = $item->getIp();
				$new_domainDB = array(
					'ip'        => $ip,
					'is_ip_ban' => false
				);

				$respIP       = $checkDC->checkAntizapretSite( $ip );

				if ( isset( $respIP->register ) || strpos( $file, $ip ) ) {
					$new_domainDB['is_ip_ban'] = true;
				}

				if ( $item->getIsIpBan() != $new_domainDB['is_ip_ban'] ) {
					$item->setIsIpBan( $new_domainDB['is_ip_ban'] );
					$resultArr[array_search($item->getPhone(), array_column($resultArr, 'phone_id'))]['message'][] = $item->getId();
					$needSendMessage    = true;

					$em->persist( $item );
					$em->flush();
				}
			}
		}
		if ($needSendMessage) {
			$checkDC->sendMessageWithBan( $resultArr );
		}
	}
}
