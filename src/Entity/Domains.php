<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomainsRepository")
 * @ORM\Table(name="domains")
 */
class Domains
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string")
	 */
    private $url;

	/**
	 * @ORM\Column(type="string")
	 */
    private $ip;

	/**
	 * @ORM\Column(type="boolean")
	 */
    private $is_ip_ban;

	/**
	 * @ORM\Column(type="boolean")
	 */
    private $is_domain_ban;

	/**
	 * @ORM\Column(type="boolean", options={"default" = 1}))
	 */
    private $track;

	/**
	 * @ORM\Column(type="boolean", options={"default" = 1}))
	 */
    private $track_ip;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $phone;

	/**
	 * @return mixed
	 */
	public function getTrack() {
		return $this->track;
	}

	/**
	 * @param mixed $track
	 */
	public function setTrack( $track ): void {
		$this->track = $track;
	}

	/**
	 * @return mixed
	 */
	public function getTrackIp() {
		return $this->track_ip;
	}

	/**
	 * @param mixed $track_ip
	 */
	public function setTrackIp( $track_ip ): void {
		$this->track_ip = $track_ip;
	}

	/**
	 * @return mixed
	 */
	public function getTrackDomain() {
		return $this->track_domain;
	}

	/**
	 * @param mixed $track_domain
	 */
	public function setTrackDomain( $track_domain ): void {
		$this->track_domain = $track_domain;
	}

	/**
	 * @ORM\Column(type="boolean", options={"default" = 1}))
	 */
    private $track_domain;

	/**
	 * @return mixed
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * @param mixed $category
	 */
	public function setCategory( $category ): void {
		$this->category = $category;
	}

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
    private $category;
	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return Domains
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @param mixed $url
	 *
	 * @return Domains
	 */
	public function setUrl( $url ) {
		$this->url = $url;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * @param mixed $ip
	 *
	 * @return Domains
	 */
	public function setIp( $ip ) {
		$this->ip = $ip;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getisIpBan() {
		return $this->is_ip_ban;
	}

	/**
	 * @param mixed $is_ip_ban
	 *
	 * @return Domains
	 */
	public function setIsIpBan( $is_ip_ban ) {
		$this->is_ip_ban = $is_ip_ban;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param mixed $phone
	 */
	public function setPhone( $phone ): void {
		$this->phone = $phone;
	}

	/**
	 * @return mixed
	 */
	public function getIsDomainBan() {
		return $this->is_domain_ban;
	}

	/**
	 * @param mixed $is_domain_ban
	 *
	 * @return Domains
	 */
	public function setIsDomainBan( $is_domain_ban ) {
		$this->is_domain_ban = $is_domain_ban;

		return $this;
	}



}
