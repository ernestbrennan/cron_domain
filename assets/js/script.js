$(function () {
    var active = "active",
        btnOpenAdress = $("#btn_addAdress"),
        fullFieldAdress = $(".bl_addAdress"),
        btnCloseAdress = $("#btn_close__addAdress"),

        btnOpenFilters = $("#btn_filters"),
        fullFieldFilters = $(".bl_filters"),
        btnCloseFilters = $("#btn_close__filters");
    function openField(btn, field) {
        btn.on("click", function () {
            $(this).addClass("hidden");
            field.addClass(active);
        })
    }
    openField(btnOpenAdress, fullFieldAdress);
    openField(btnOpenFilters, fullFieldFilters);
    function closeField(btn, field, visionBtn) {
        btn.on("click", function () {
            field.removeClass(active);
            visionBtn.removeClass("hidden");
        })
    }
    closeField(btnCloseAdress, fullFieldAdress, btnOpenAdress);
    closeField(btnCloseFilters, fullFieldFilters, btnOpenFilters);
    var lableChoothAllCheckbox = $(".allCheckbox");
    lableChoothAllCheckbox.on("click", function(){
        var choothAllCheckbox = $(".bl_categories__item:not(.allCheckbox)").find(":checkbox"),
            choothAllChackbox = $(".allCheckbox").children(":checkbox");

        if( choothAllChackbox.prop("checked") === true){
            choothAllCheckbox.prop('checked', true);
        }else{
            choothAllCheckbox.prop('checked', false);
        }

    })
});

$('#addNewDomain').on('click', function (e) {
    var domain_url      = $('.addItAdress').val();
    var category_id     = $(".select_chooseCategory option:selected").val();
    var category_name   = $(".select_chooseCategory option:selected").val() != 'null' ? $(".select_chooseCategory option:selected").text() : '';
    var phone_id        = $(".select_choosePhone option:selected").val();
    var phone_number    = $(".select_choosePhone option:selected").val() != 'null' ? $(".select_choosePhone option:selected").text() : '';

    console.log(domain_url)

    $.ajax({
        url: "/add-domain",
        type: 'post',
        data: {
            'domain_url'    : domain_url,
            'category_id'   : category_id,
            'phone_id'      : phone_id
        },
        success: function (data) {
            var status = '';
            if (data.is_domain_ban && data.is_ip_ban) {
                status = 'domain-ban  ip-ban'
            } else if (data.is_domain_ban) {
                status = 'domain-ban'
            } else if (data.is_ip_ban) {
                status = 'ip-ban'
            } else {
                status = 'ok'
            }
            $('#table_banControlSystem')
                .append(
                    '<tr><td>' + data.id + '</td>' +
                    '<td>' +
                    '<a class="ban_domain" href="//' + data.url + '" target="_blank">' + data.url + '</a>' +
                    '</td>' +
                    '<td>' +
                    '<span class="ban_value">' + status + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<span class="ban_ip">' + data.ip + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<span class="ban_ip">' + category_name + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<span >' + phone_number + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<label class="ban_track">' +
                    '<input class="ban_checkbox" type="checkbox">' +
                    '<span class="ban_checkbox__vision"></span>' +
                    '</label>' +
                    '</td>' +
                    '<td>' +
                    '<label class="ban_domainAlert">' +
                    '<input class="ban_checkbox" type="checkbox">' +
                    '<span class="ban_checkbox__vision"></span>' +
                    '</label>' +
                    '</td>' +
                    '<td>' +
                    '<label class="ban_IPAlert">' +
                    '<input class="ban_checkbox" type="checkbox">' +
                    '<span class="ban_checkbox__vision"></span>' +
                    '</label>' +
                    '</td>' +
                    '<td>' +
                    '<button class="btn_delite" type="button" domain_id="' + data.id + '"></button>' +
                    '</td>' +
                    '</tr>');
            console.log(data);
        }
    });
});

$('body').on('click', ".btn_delite", function (e) {
    var th = $(this)
    $.ajax({
        url: "/delete-domain",
        type: 'post',
        data: 'domain_id=' + th.attr('domain_id'),
        success: function (data) {
            th.parent().parent().remove()
            console.log(data)
        }
    })
})

$('#addNewCategory').on('click', function (e) {
    var category_name   = $('.addItCategorie').val()
    var selectList      = $('.select_chooseCategory');
    var filterList      = $('#categoryFilter');
    var tableRowLists   = $('.select_domain_category');
    $.ajax({
        url: "/add-category",
        type: 'post',
        data: 'category_name=' + category_name,
        success: function (data) {
            selectList.append('<option value="' + data + '">' + category_name + '</option>');
            filterList.append('<div class="bl_categories__item">' +
                '<label class="bl_categories__item">' +
                '<input type="checkbox" name="checkbox_categories">' +
                '<span>' + category_name + '</span>' +
                '</label>' +
                '<button class="btn_categories__delite" type="button" category_id="' + data + '"></button>' +
                '</div>');
            tableRowLists.each(function(index, value){
                $(this).append($("<option/>", {
                    value:  data,
                    text:   category_name
                }));
            })
        }
    })
})
$('#addNewPhone').on('click', function (e) {
    var phoneNumber     = $('.addItPhone').val()
    var selectList      = $('.select_choosePhone');
    var filterList      = $('#phoneFilter');
    var tableRowLists    = $('.select_domain_phone');
    $.ajax({
        url: "/add-phone",
        type: 'post',
        data: 'phone_number=' + phoneNumber,
        success: function (data) {
            selectList.append('<option value="' + data + '">' + phoneNumber + '</option>');
            filterList.append('<div class="bl_categories__item">' +
                '<label class="bl_categories__item">' +
                '<input type="checkbox" name="checkbox_categories">' +
                '<span>' + phoneNumber + '</span>' +
                '</label>' +
                '<button class="btn_categories__delite" type="button" category_id="' + data + '"></button>' +
                '</div>');
            tableRowLists.each(function(index, value){
                $(this).append($("<option/>", {
                    value: data,
                    text: phoneNumber
                }));
            })

        }
    })
})

$('[delete_category]').on('click', function (e) {
    var delBtn              = $(this);
    var categoryId          = delBtn.attr('category_id')
    var selectList          = $('.select_chooseCategory');
    var tableRowLists       = $('.select_domain_category');

    $.ajax({
        url: "/delete-category",
        type: 'post',
        data: 'category_id=' + categoryId,
        success: function (data) {
            $("[span_dom_cat=" + categoryId + "]").text('').append('' +
                '');
            $('.select_chooseCategory option[value="' + categoryId + '"]').remove()
            delBtn.parent().remove()
            tableRowLists.each(function(index, value){
                $(this).find('[value="'+categoryId +'"]').remove();
            })
        }
    });
});
$('[phone_delete]').on('click', function (e) {
    var delBtn              = $(this);
    var phoneId             = delBtn.attr('phone_id')
    var selectList          = $('.select_choosePhone');
    var tableRowLists       = $('.select_domain_phone');

    $.ajax({
        url: "/delete-phone",
        type: 'post',
        data: 'phone_id=' + phoneId,
        success: function (data) {

                $("[span_dom_phone=" + phoneId + "]").text('');


            $('.select_choosePhone option[value="' + phoneId + '"]').remove()
            delBtn.parent().remove()
            tableRowLists.each(function(index, value){
                $(this).find('[value="'+phoneId +'"]').remove();
            })
        }
    });
});


$('.select_domain_phone').on('change', function () {
    var select          = $(this)
    var phoneId         = this.value;
    var domainId        = select.attr('domain_id')
    var phoneNumber     = select.find("option:selected").text()
    $.ajax({
        url: "/add-phone-to-domain",
        type: 'post',
        data: {
            'phone_id'  : phoneId,
            'domain_id' : domainId
        },
        success: function (data) {
            select.parent().attr('span_dom_phone', phoneId)
            select.parent().text(phoneNumber)
            select.remove()
        }
    });
});
$('.select_domain_category').on('change', function () {
    var select = $(this)
    var categoryId = this.value;
    var domainId = $(this).attr('domain_id')
    var categoryName = $(this).find("option:selected").text()
    $.ajax({
        url: "/add-category-to-domain",
        type: 'post',
        data: 'category_id=' + categoryId + '&domain_id=' + domainId,
        success: function (data) {
            select.parent().attr('span_dom_cat', categoryId)
            select.parent().text(categoryName)
            select.remove()
        }
    });
});

$('[track]').on('click', function(){
    $.ajax({
        url: "/track",
        type: 'post',
        data: 'track=' + $(this).attr('track'),
        success: function (data) {
            console.log(data)
        }
    })
});

$('[track_domain]').on('click', function(){
    $.ajax({
        url: "/track-domain",
        type: 'post',
        data: 'track_domain=' + $(this).attr('track_domain'),
        success: function (data) {
            console.log(data)
        }
    })
});

$('[track_ip]').on('click', function(){
    $.ajax({
        url: "/track-ip",
        type: 'post',
        data: 'track_ip=' + $(this).attr('track_ip'),
        success: function (data) {
            console.log(data)
        }
    })
});

$('[filterStatus]').on('click', function () {

    var filterStatus        = $(this).attr('filterStatus');
    var domainsTBody    = $('#table_banControlSystem tbody');
    $('[filterStatus]').prop('checked', false);
    $(this).prop('checked', true);


    var url     = '';
    var props   = '';

    if(filterStatus === 'all'){
        url     = '/get-all-domain';
    } else if(filterStatus === 'category'){
        url     = '/get-by-category';
        props   = {
            'category_id': $(this).attr("category_id")
        }
    } else {
        url     = '/get-by-phone';
        props   = {
            'phone_id': $(this).attr("phone_id")
        }
    }

    $.ajax({
        url: url,
        type: 'post',
        data: props,
        success: function (data) {
            console.log(data)
            domainsTBody.find('tr').remove()
            data.forEach(function (item) {
                var status = '';
                if (item.ban_domain && item.ban_ip) {
                    status = 'domain-ban  ip-ban'
                } else if (item.ban_domain) {
                    status = 'domain-ban'
                } else if (item.ban_ip) {
                    status = 'ip-ban'
                } else {
                    status = 'ok'
                }
                var row = `
                    <tr>
                        <td>${item.id}</td>
                        <td>
                            <a class="ban_domain" href="//${item.url}" target="_blank">${item.url}</a>
                        </td>
                        <td>
                            <span class="ban_value">${status}</span>
                        </td>
                        <td>
                            <span class="ban_ip">${item.ip}</span>
                        </td>
                        <td>
                            <span class="ban_ip" span_dom_cat=${item.category_id}>
                                ${item.category_name}
                            </span>
                        </td>
                        <td>
                            <span  span_dom_phone=${item.phone_id}>
                                ${item.phone_number}
                            </span>
                        </td>
                        <td>
                            <label class="ban_track">
                                <input class="ban_checkbox" type="checkbox" ${ item.track ? 'checked' : ''}>
                                <span class="ban_checkbox__vision"></span>
                            </label>
                        </td>
                        <td><label class="ban_domainAlert">
                                <input class="ban_checkbox" type="checkbox" ${ item.track_ip ? 'checked' : ''}>
                                <span class="ban_checkbox__vision"></span>
                            </label>
                        </td>
                        <td><label class="ban_IPAlert">
                                <input class="ban_checkbox" type="checkbox" ${ item.track_domain ? 'checked' : ''}>
                                <span class="ban_checkbox__vision"></span>
                            </label></td>
                        <td>
                        <button class="btn_delite choimg" type="button" domain_id="${item.id}"></button>
                        </td>
                    </tr>`;
                domainsTBody.append(row);
            })
        }
    });
});